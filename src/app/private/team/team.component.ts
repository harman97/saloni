import { Component, OnInit } from '@angular/core';
import { TeamService } from "../../team.service";
import { TotalService } from '~/app/services/total.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-team',
  templateUrl: './team.component.html',
  styleUrls: ['./team.component.css']
})
export class TeamComponent implements OnInit {

  constructor(public teamService: TeamService, private sum: TotalService, private router: Router) { }

  teams: any = [];
  total: number;
  id: number;

  ngOnInit() {
    this.teamService.getTeams().subscribe(res => {
      this.teams = res.data;
      console.log(res);
      this.total = res.data.reduce((sum, item) => sum + item.amount, 0);
      this.sum.count = this.total;
    })
  }

  delete(team) {
    console.log("hi");
    console.log(team.team_id);
    this.id = team.team_id;
    let data = {
      team_id: this.id
    }

    if (confirm("Are you sure you want to delete team")) {
      this.teamService.deleteTeam(data).subscribe(response => {
        console.log(response);
        console.log(response.status);
        if (response && response.status == 200) {
          console.log("in");
          window.location.reload();
        }
      });
    } else {
      console.log('cancel');
    }

  }

  edit() {
    console.log("edit hua");
    this.router.navigate(['private/team/editTeam']);
  }
}
